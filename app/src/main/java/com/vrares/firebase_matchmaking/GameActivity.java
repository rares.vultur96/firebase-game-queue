package com.vrares.firebase_matchmaking;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;

public class GameActivity extends AppCompatActivity {

    private static final String QUEUE_NODE = "queue";
    public static final String USERS_NODE = "users";
    private static final String GAMES_NODE = "games";
    private static final String STATUS_NODE= "status";
    private static final String PLAYER_1_NODE = "player1";
    private static final String PLAYER_2_NODE = "player2";
    private static final String EMPTY_QUEUE = "Empty queue";

    private User localUser;
    private User remoteUser;

    private DatabaseReference dbRoot;
    private DatabaseReference dbQueue;
    private DatabaseReference dbGames;
    private DatabaseReference dbUsers;

    private Button btnJoinGame;
    private TextView tvStatus;

    private String matchKey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        btnJoinGame = findViewById(R.id.game_btn);
        tvStatus = findViewById(R.id.game_tv);
        dbRoot = FirebaseDatabase.getInstance().getReference();
        dbQueue = FirebaseDatabase.getInstance().getReference(QUEUE_NODE);
        dbGames = FirebaseDatabase.getInstance().getReference(GAMES_NODE);
        dbUsers = FirebaseDatabase.getInstance().getReference(USERS_NODE);

        dbRoot.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (!dataSnapshot.hasChild(QUEUE_NODE)) {
                    dbQueue.child(STATUS_NODE).setValue(EMPTY_QUEUE);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        getUserInfo();

        btnJoinGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvStatus.setText("Searching for a player...");
                searchForAGame();
            }
        });


    }

    private void searchForAGame() {
        dbQueue.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                matchKey = dataSnapshot.child(STATUS_NODE).getValue().toString();
                if (dataSnapshot.child(STATUS_NODE).getValue().toString().equals(EMPTY_QUEUE)) {
                    enterQueue();
                } else {
                    joinQueue();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void joinQueue() {
        dbUsers.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                localUser = dataSnapshot.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).getValue(User.class);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        dbQueue.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {

                if (mutableData.child(STATUS_NODE).getValue().toString().equals(matchKey)) {
                    mutableData.child(STATUS_NODE).setValue(EMPTY_QUEUE);
                    return Transaction.success(mutableData);
                }

                return Transaction.abort();
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean result, DataSnapshot dataSnapshot) {
                if (result) {
                    dbGames.child(matchKey).child(PLAYER_2_NODE).setValue(localUser);
                    dbGames.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            remoteUser = dataSnapshot.child(matchKey).child(PLAYER_1_NODE).getValue(User.class);
                            tvStatus.setText(localUser.getEmail() + " vs " + remoteUser.getEmail());
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Log.d("Error", databaseError.getMessage());
                        }
                    });
                } else {
                    searchForAGame();
                }
            }
        });
    }

    private void enterQueue() {

        dbQueue.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                if (mutableData.child(STATUS_NODE).getValue().toString().equals(EMPTY_QUEUE)) {
                    matchKey = dbQueue.push().getKey();
                    mutableData.child(STATUS_NODE).setValue(matchKey);
                    return Transaction.success(mutableData);
                }

                return Transaction.abort();
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean result, DataSnapshot dataSnapshot) {
                if (result) {
                    dbGames.child(matchKey).child(PLAYER_1_NODE).setValue(localUser);
                    dbGames.child(matchKey).addChildEventListener(new ChildEventListener() {
                        @Override
                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                            if (dataSnapshot.getKey().equals(PLAYER_2_NODE)) {
                                remoteUser = dataSnapshot.getValue(User.class);
                                tvStatus.setText(localUser.getEmail() + " vs " + remoteUser.getEmail());
                            }
                        }

                        @Override
                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        }

                        @Override
                        public void onChildRemoved(DataSnapshot dataSnapshot) {

                        }

                        @Override
                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                } else {
                    searchForAGame();
                }
            }
        });
    }

    private void getUserInfo() {
        dbUsers.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                localUser = dataSnapshot.getValue(User.class);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
